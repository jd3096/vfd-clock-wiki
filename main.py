#我承认我写了一坨屎山，如果您想从我这里学习写代码，那无疑我是个反面教材，我不是故意的，下个项目我将努力吸取这个项目的经验，少造屎山
#非尝试修改程序不建议尝试读懂此屎山
#自己造的屎自己吃，虽然屎山，但是功能不差，有问题我也会及时修正，欢迎联系我。vx:jd3096
from machine import Timer
from CLOCK import VFDCLOCK
import time,machine,gc
from tools import song1,song2,song3,song4,song5,song6,song7,song8
from drivers import music
from time import sleep
from machine import Pin,PWM

def menu():
    clock.btcb=0
    clock.clock_timer=False
    while 1:
        try:
            clock.texts(clock.menu[clock.func])
        except:
            clock.texts(clock.menu[clock.func][0])
        time.sleep_ms(30)
        if clock.btcb!=0:
            if clock.btcb==1 or clock.btcb==11:
                if clock.func==0:
                    clock.func=clock.menu_len-1
                else:
                    clock.func-=1
                clock.scroll_menu(clock.func,True)
            elif clock.btcb==2:
                break
            elif clock.btcb==3 or clock.btcb==13:
                if clock.func==clock.menu_len-1:
                    clock.func=0
                else:
                    clock.func+=1
                clock.scroll_menu(clock.func,False)
            clock.btcb=0
        else:
            time.sleep_ms(30)
    if clock.func==0:
        datetime()
    elif clock.func==1:
        alarm()
    elif clock.func==2:
        pomodoro()
    elif clock.func==3:
        settings()
    elif clock.func==4:
        playmusic()
    elif clock.func==5:
        info()
    elif clock.func==7:
        tim.deinit()
        if clock.OTA():
            time.sleep(2)
            clock.texts('RESET 3')
            time.sleep(1)
            clock.texts('RESET 2')
            time.sleep(1)
            clock.texts('RESET 1')
            time.sleep(1)
            machine.reset()
        else:
            menu()

def pdmcalc(t):
    global pdtime,pds,work,pause
    if not pause:
        if pdtime!=0:
            pdtime-=1
            mm=str(pdtime//60)
            ss=str(pdtime%60)
            if len(mm)==1:
                mm='0'+mm
            if len(ss)==1:
                ss='0'+ss
            print(mm,ss)
            clock.texts(pds+mm+':'+ss)

def pomodoro():
    global pdtime,work,pds,pause
    pause=False
    pds='WO '
    work=True
    pdtime=25*60
    pdm=Timer(1)
    pdm.init(period=1000, callback=pdmcalc)
    clock.btcb=0
    while 1:
        if clock.btcb==22:
            break
        if clock.btcb==2:
            pause=not pause
            clock.btcb=0
        if pdtime==0:
            if work:
                while 1:
                    if clock.btcb==2:
                        break
                    selectsong(clock.alarm_song)
                clock.btcb=0
                work=0
                pdtime=5*60
                pds='BR '
                
            else:
                while 1:
                    if clock.btcb==2:
                        break
                    selectsong(clock.alarm_song)
                clock.btcb=0
                work=1
                pdtime=25*60
                pds='WO '
    clock.btcb=0
    pdm.deinit()
    menu()
    
            
def alarm():
    clock.clock_timer=False
    clock.btcb=0
    hh=str(clock.alarm_h)
    mm=str(clock.alarm_m)
    if len(hh)==1:
        hh='0'+hh
    if len(mm)==1:
        mm='0'+mm
    while True:
        if clock.btcb==22 or clock.btcb==2:
            break
        if clock.btcb==3:
            if clock.alarm_h==23:
                pass
            else:
                clock.alarm_h+=1
            clock.btcb=0
        elif clock.btcb==1:
            if clock.alarm_h==0:
                pass
            else:
                clock.alarm_h-=1
            clock.btcb=0
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if clock.alarm_h==24:
                        pass
                    else:
                        clock.alarm_h+=1
                    clock.texts('HOUR:'+str(clock.alarm_h))
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if clock.alarm_h==0:
                        pass
                    else:
                        clock.alarm_h-=1
                    clock.texts('HOUR:'+str(clock.alarm_h))
            clock.btcb=0
        clock.texts('HOUR:'+str(clock.alarm_h))
    if clock.btcb==2:
        clock.btcb=0
    while True:
        if clock.btcb==22 or clock.btcb==2:
            break
        if clock.btcb==3:
            if clock.alarm_m==59:
                pass
            else:
                clock.alarm_m+=1
            clock.btcb=0
        elif clock.btcb==1:
            if clock.alarm_m==0:
                pass
            else:
                clock.alarm_m-=1
            clock.btcb=0
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if clock.alarm_m==59:
                        pass
                    else:
                        clock.alarm_m+=1
                    clock.texts('MINU:'+str(clock.alarm_m))
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if clock.alarm_m==0:
                        pass
                    else:
                        clock.alarm_m-=1
                    clock.texts('MINU:'+str(clock.alarm_m))
            clock.btcb=0
        clock.texts('MINU:'+str(clock.alarm_m))
    if clock.btcb==2:
        clock.btcb=0
    while True:
        if clock.btcb==22 or clock.btcb==2:
            break
        if clock.btcb==3 or clock.btcb==1:
            clock.alarm=not clock.alarm
            clock.btcb=0
        if clock.alarm:
            clock.texts(' On     ')
        else:
            clock.texts('    off ')
    if clock.btcb!=22:
        clock.texts(" SAVED!")
        clock.save()
        time.sleep(1)
    menu()

def settings():
    clock.btcb=0
    clock.clock_timer=False
    while 1:
        clock.texts(clock.menu[3][clock.func_s])
        time.sleep_ms(30)
        if clock.btcb!=0:
            if clock.btcb==1 or clock.btcb==11:
                if clock.func_s==1:
                    clock.func_s=clock.set_len-1
                else:
                    clock.func_s-=1
                clock.scroll_set(clock.func_s,True)
            elif clock.btcb==2 or clock.btcb==22:
                break
            elif clock.btcb==3 or clock.btcb==13:
                if clock.func_s==clock.set_len-1:
                    clock.func_s=1
                else:
                    clock.func_s+=1
                clock.scroll_set(clock.func_s,False)
            clock.btcb=0
        else:
            time.sleep_ms(30)
    if clock.btcb==22:
        menu()
    if clock.func_s==1:
        countday()
    elif clock.func_s==2:
        timezone()
    elif clock.func_s==3:
        ntptime()
    elif clock.func_s==4:
        backlight()
    elif clock.func_s==5:
        airkiss()
    elif clock.func_s==6:
        tim.deinit()
        if clock.OTA():
            time.sleep(2)
            clock.texts('RESET 3')
            time.sleep(1)
            clock.texts('RESET 2')
            time.sleep(1)
            clock.texts('RESET 1')
            time.sleep(1)
            machine.reset()
        else:
            settings()
            
def datetime():
    gc.collect()
    clock.clock_timer=True
    clock.btcb=0
    while 1:
        time.sleep_ms(50)
        if clock.btcb==22 or clock.btcb==2:
            break
        elif clock.btcb==1:
            clock.blink()
            while 1:
                time.sleep_ms(20)
                if clock.btcb==3 or clock.btcb==22 or clock.btcb==2:
                    break
                clock.clock_timer=False
                clock.show_date()
            if clock.btcb==3:
                clock.btcb=0
                clock.clock_timer=True
            clock.blink()
        elif clock.btcb==3:
            clock.blink()
            clock.display.display_str(0,'        ')
            while 1:
                time.sleep_ms(20)
                if clock.btcb==1 or clock.btcb==22 or clock.btcb==2:
                    break
                clock.clock_timer=False
                clock.count_day()
            if clock.btcb==1:
                clock.btcb=0
                clock.clock_timer=True
            clock.blink()
        else:
            pass
    if clock.btcb==22 or clock.btcb==2:
        menu()
    clock.clock_timer=False
    
def countday():
    clock.btcb=0
    yy=clock.countday[0]
    mm=clock.countday[1]
    dd=clock.countday[2]
    clock.texts('YEAR'+str(yy))
    while 1:
        if clock.btcb==2:
            break
        elif clock.btcb==1:
            if yy>2023:
                yy-=1
        elif clock.btcb==3:
            if yy<2100:
                yy+=1
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if yy<2100:
                        yy+=1
                    clock.texts('YEAR'+str(yy))
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if yy>2023:
                        yy-=1
                    clock.texts('YEAR'+str(yy))
            clock.btcb=0
        clock.btcb=0
        clock.texts('YEAR'+str(yy))
    clock.texts('MONTH:'+str(mm))
    clock.btcb=0
    while 1:
        if clock.btcb==2:
            break
        elif clock.btcb==1:
            if mm>1:
                mm-=1
        elif clock.btcb==3:
            if mm<12:
                mm+=1
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if mm<12:
                        mm+=1
                    clock.texts('MONTH:'+str(mm))
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if mm>1:
                        mm-=1
                    clock.texts('MONTH:'+str(mm))
            clock.btcb=0
        clock.btcb=0
        clock.texts('MONTH:'+str(mm))
    clock.texts(' DAY:'+str(dd))
    clock.btcb=0
    if mm==1 or mm==3 or mm==5 or mm==7 or mm==8 or mm==10 or mm==12:
        d_max=31
    elif mm==2:
        if (yy % 4) == 0:
           if (yy % 100) == 0:
               if (yy % 400) == 0:
                   d_max=29  
               else:
                   d_max=28
           else:
               d_max=29     
        else:
           d_max=28
    else:
        d_max=30
    while 1:
        if clock.btcb==2:
            break
        elif clock.btcb==1:
            if dd>1:
                dd-=1
        elif clock.btcb==3:
            if dd<d_max:
                dd+=1
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if dd<d_max:
                        dd+=1
                    clock.texts(' DAY:'+str(dd))
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if dd>1:
                        dd-=1
                    clock.texts(' DAY:'+str(dd))
            clock.btcb=0
        clock.btcb=0
        clock.texts(' DAY:'+str(dd))
    clock.texts(' SAVED!')
    clock.countday=(yy,mm,dd,0,0,0,0,0)
    clock.save()
    time.sleep(1)
    settings()
    
def ntptime():
    clock.ntp_get()
    time.sleep(1)
    settings()
    
def bl_set():
    if clock.bl==0 or clock.bl==255:
        clock.display.set_display_dimming(100)
        clock.texts('BL:'+' '+'AUTO')
    else:
        clock.texts('BL:'+' '+str(clock.bl))
        clock.display.set_display_dimming(clock.bl)
    
def backlight():
    clock.btcb=0
    clock.texts('BL:'+' '+str(clock.bl))
    max_val=255
    while True:
        if clock.btcb==22 or clock.btcb==2:
            break
        if clock.btcb==3:
            if clock.bl==max_val:
                pass
            else:
                clock.bl+=1
            clock.btcb=0
        elif clock.btcb==1:
            if clock.bl==0:
                pass
            else:
                clock.bl-=1
            clock.btcb=0
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if clock.bl==max_val:
                        pass
                    else:
                        clock.bl+=1
                    bl_set()
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if clock.bl==0:
                        pass
                    else:
                        clock.bl-=1
                    bl_set()
            clock.btcb=0

    clock.texts(" SAVED!")
    clock.bl=clock.bl
    clock.save()
    time.sleep(1)
    settings()
    
def timezone():
    clock.btcb=0
    tzstr=['  UTC',' UTC+1',' UTC+2',' UTC+3',' UTC+4',' UTC+5',' UTC+6',' UTC+7',' UTC+8',' UTC+9',' UTC+10',' UTC+11',' UTC+12',' UTC-1',' UTC-2',' UTC-3',' UTC-4',' UTC-5',' UTC-6',' UTC-7',' UTC-8',' UTC-9',' UTC-10',' UTC-11',' UTC-12']
    tznow=clock.timezone
    clock.texts(tzstr[tznow])
    while 1:
        if clock.btcb==2:
            break
        elif clock.btcb==1:
            if tznow>0:
                tznow-=1
        elif clock.btcb==3:
            if tznow<24:
                tznow+=1
        elif clock.btcb==23:
            while 1:
                if clock.btcb==33:
                    break
                else:
                    if tznow<24:
                        tznow+=1
                        clock.texts(tzstr[tznow])
            clock.btcb=0
        elif clock.btcb==21:
            while 1:
                if clock.btcb==31:
                    break
                else:
                    if tznow>0:
                        tznow-=1
                        clock.texts(tzstr[tznow])
            clock.btcb=0
        clock.btcb=0
        clock.texts(tzstr[tznow])
    clock.texts(' SAVED!')
    clock.timezone=tznow
    clock.save()
    time.sleep(1)
    ntptime()

def airkiss():
    clock.ble_ap()
    time.sleep(2)
    clock.texts('RESET 3')
    time.sleep(1)
    clock.texts('RESET 2')
    time.sleep(1)
    clock.texts('RESET 1')
    time.sleep(1)
    machine.reset()
    
def info():
    clock.info()
    menu()

def playsong(song,speed):
    mySong = music(song, pins=[Pin(8)])
    pp=0
    if clock.btcb==2:
        print('2b')
        pp=True
    clock.btcb=0
    while True:
        if clock.btcb==1 or clock.btcb==3 or clock.btcb==22 or clock.btcb==2:
            break
        mySong.tick()
        sleep(speed)
    pwm=PWM(Pin(8))
    pwm.deinit()
    if pp:
        clock.btcb=2
        pp=0

def selectsong(select):
    if select==0:
        playsong(song1,0.035)
    elif select==1:
        playsong(song2,0.03)
    elif select==2:
        playsong(song3,0.055)
    elif select==3:
        playsong(song4,0.033)
    elif select==4:
        playsong(song5,0.034)
    elif select==5:
        playsong(song6,0.036)
    elif select==6:
        playsong(song7,0.028)
    elif select==7:
        playsong(song8,0.043)
        
def playmusic():
    clock.btcb=0
    songlist=['Tetris','Tele','Fantasy','Chocobo','JOJO','Castle','Mario','Nokia']
    clock.texts(songlist[clock.alarm_song])
    clock.display.display_str(7,'\x0a')
    selectsong(clock.alarm_song)
    select=clock.alarm_song
    
    while 1:
        time.sleep_ms(2)
        if clock.btcb==3:
            if select==len(songlist)-1:
                select=0
            else:
                select+=1
            clock.texts(songlist[select])
            if select==clock.alarm_song:
                clock.display.display_str(7,'\x0a')
            selectsong(select)
        elif clock.btcb==1:
            if select==0:
                select=len(songlist)-1
            else:
                select-=1
            clock.texts(songlist[select])
            if select==clock.alarm_song:
                clock.display.display_str(7,'\x0a')
            selectsong(select)
        elif clock.btcb==2:
            clock.alarm_song=select
            clock.display.display_str(7,'\x0a')
            clock.save()
            clock.btcb=0
        elif clock.btcb==22:
            break
    clock.btcb=0
    menu()
            

#---------MAIN PROGRAM------------
clock=VFDCLOCK()
clock.ani_x_fly('VFD PRO ')
clock.blink()
#clock.rtc2esp()
#clock.esp2rtc()
#clock.ntp_slience()
if clock.rtc2esp():
    pass
else:
    clock.ntp_get()
tim=Timer(-1)
tim.init(period=1000, callback=clock.show_time)

datetime()





















